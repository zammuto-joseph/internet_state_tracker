import socket
import time
from multiprocessing import Process
from datetime import datetime
from os import system


class Listener:
    def __init__(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(('', 8080))
        sock.listen(5)
        print('Server started. Listening on port 8080...')
        while True:
            connection, client_address = sock.accept()
            print('Heartbeat received from client at IP address: '
                  f'{client_address[0]}')
            file_handle = open('heartbeats.dat', 'w')
            now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
            file_handle.write(f'[{now}] Heartbeat received from '
                              f'{client_address[0]}')
            file_handle.close()


class Notifier:
    def __init__(self):
        last_known_state = True
        current_state = True
        while True:
            now = datetime.now()
            with open('heartbeats.dat', 'r') as file_handle:
                heartbeat = ''
                for heartbeat in file_handle.readlines():
                    pass
                heartbeat_datetime = datetime.strptime(heartbeat[1:20],
                                                       '%m-%d-%Y %H:%M:%S')
                time_delta = now - heartbeat_datetime
                if time_delta.seconds > 60:
                    current_state = False
                else:
                    current_state = True
                if last_known_state != current_state:
                    last_known_state = current_state
                    if current_state:
                        notification_message = \
                            'A heartbeat has been received. Your internet ' \
                            'connection is restored.'
                    else:
                        notification_message = \
                            'No heartbeat has been received for more ' \
                            'than 1 minute. Your internet connection ' \
                            'may have gone down.'
                    print(f'{notification_message}\n')
                    system('mail -aFrom:notifications@jzammuto.net '
                           '-aReply-To:notifications@jzammuto.net -s '
                           '"Internet Connection Status Alert" '
                           '''Target Emails Redacted below. Please change before implementing.
                           '''
                           'example@domain.com '
                           f'<<< "{notification_message}"')
            time.sleep(30)


def start_listener():
    listener = Listener()


def start_notifier():
    notifier = Notifier()


if __name__ == '__main__':
    Process(target=start_listener).start()
    time.sleep(2)
    Process(target=start_notifier).start()
