import socket
import time


if __name__ == '__main__':
    while True:
        sock = socket.socket()
        server_address = ('127.0.0.1', 8080)
        sock.connect(server_address)
        message_string = '1'
        sock.sendall(bytes(message_string, 'utf-8'))
        sock.close()
        time.sleep(45)